package ci.web.util.file;

import java.io.File;

/**
 * 监控器侦听接口
 * @author zhangheng
 */
public interface FsHandler {
    /**
     * 创建file
     * @param file
     */
	void onCreate(File file);
	/**
	 * 修改file
	 * @param file
	 */
	void onModify(File file);
	/**
	 * 删除file
	 * @param file
	 */
	void onDelete(File file);
}
