package ci.web.core;

import java.net.SocketAddress;
import java.util.List;
import java.util.Set;

import ci.web.HttpMethod;
import ci.web.codec.FileItem;

import com.alibaba.fastjson.JSONObject;

import io.netty.handler.codec.http.Cookie;
import io.netty.handler.codec.http.HttpHeaders;

/**
 * 请求
 * @author zhh
 */
public interface CiRequest {

    /**
     * 请求HttpMethod
     * @return
     */
    HttpMethod method();
    /**
     * http-refer-host
     * @return 
     */
    String refererHost();
    /**
     * 请求http-host
     * @return
     */
    String host();
    /**
     * 请求uri
     * @return
     */
    String uri();
    /**
     * 请求路径
     * @return
     */
    String path();
    /**
     * 来源remote_address
     * @return
     */
    SocketAddress address();
    /**
     * 请求参数
     * @return
     */
    JSONObject params();
    /**
     * post的参数
     * @return
     */
    JSONObject post();
    /**
     * get的参数
     * @return
     */
    JSONObject get();
    
    /**
     * http请求的content
     * @return
     */
    byte[] body();
    
    /**
     * 上传的文件
     * @return
     */
    List<FileItem> files();
    /**
     * 上传的文件
     * @param name
     * @return
     */
    FileItem getFile(String name);
    /**
     * 请求http-headers
     * @return
     */
    HttpHeaders headers();
    /**
     * 获取header
     * @param name
     * @return
     */
    String getHeader(CharSequence name);
    /**
     * 获取cookie
     * @return
     */
    Set<Cookie> cookies();
    /**
     * 获取cookie
     * @return
     */
    Cookie getCookie(CharSequence name);
    /**
     * 获取cookie
     * @return
     */
    String getCookieValue(CharSequence name);
    /**
     * 请求ContentType
     * @return
     */
    String contentType();
    
    /**
     * 是否结束
     * @return
     */
    boolean isFinish();
    /**
     * 结束(清理临时文件，属性等)
     */
    void finish();
    
}
