package ci.web.core;

import io.netty.channel.group.ChannelGroup;

public interface CiWebService {

    /**
     * 服务是否还在工作中
     * @return
     */
    boolean isAlive();
    /**
     * http普通请求的连接数量
     * @return
     */
    int httpConnectNum();
    /**
     * websocket的ChannelGroup
     * @return
     */
    ChannelGroup webSocketGroup();
}
