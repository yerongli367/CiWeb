package ci.web.annotaction;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import ci.web.HttpMethod;

/**
 * 限制访问HttpMethod
 * @author zhh
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Limiter {
    HttpMethod value() default HttpMethod.POST;
}
