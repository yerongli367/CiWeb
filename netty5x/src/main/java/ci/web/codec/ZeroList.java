package ci.web.codec;

import java.util.ArrayList;
import java.util.Collection;

class ZeroList<E> extends ArrayList<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8537156478384238393L;

	public ZeroList(int initialCapacity) {
		super(initialCapacity);
	}

	@Override
	public boolean add(E e) {
		return false;
	}

	@Override
	public void add(int index, E element) {
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		return false;
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		return false;
	}

}
