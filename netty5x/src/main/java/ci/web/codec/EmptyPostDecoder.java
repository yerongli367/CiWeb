package ci.web.codec;

import io.netty.handler.codec.http.FullHttpRequest;

import com.alibaba.fastjson.JSONObject;

/**
 * 不对POST进行解码的解码器
 * @author zhh
 */
public class EmptyPostDecoder extends FormPostDecoder {

	public EmptyPostDecoder(FullHttpRequest request) throws Exception {
		super(request);
	}
	@Override
	protected void decode() throws Exception{
		paramters = new JSONObject();
		bodyDatas = EMPTY_BODYDATAS;
		files = EMPTY_FILES;
	}
	
	/**
	 * don't need clean
	 */
	public void clean() {
	    
	}
	
	public static EmptyPostDecoder single(){
	    return _INSTANCE;
	}
	private static EmptyPostDecoder _INSTANCE;
	static{
	    try {
	        _INSTANCE = new EmptyPostDecoder(null);
        } catch (Exception e) {
        }
	}
}
