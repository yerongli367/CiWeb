package ci.web.codec;

import io.netty.handler.codec.http.HttpConstants;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.multipart.Attribute;
import io.netty.handler.codec.http.multipart.DefaultHttpDataFactory;
import io.netty.handler.codec.http.multipart.MemoryAttribute;

import java.io.IOException;
/**
 * 参数必定使用内存<br/>
 * 文件看构造使用磁盘/内存
 * @author zhh
 */
class WebHttpDataFactory extends DefaultHttpDataFactory {

	public WebHttpDataFactory(boolean useDisk) {
		super(useDisk);
	}
	
    @Override
    public Attribute createAttribute(HttpRequest request, String name) {
        MemoryAttribute attribute = new MemoryAttribute(name);
        attribute.setMaxSize(-1);
        return attribute;
    }
    @Override
    public Attribute createAttribute(HttpRequest request, String name, String value) {
        try {
            MemoryAttribute attribute = new MemoryAttribute(name, value, HttpConstants.DEFAULT_CHARSET);
            attribute.setMaxSize(-1);
            return attribute;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
