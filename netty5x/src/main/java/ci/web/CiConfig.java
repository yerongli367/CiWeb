package ci.web;

import com.alibaba.fastjson.JSONObject;

import ci.web.util.ConfigUtil;

/**
 * 配置
 * @author zhh
 */
public class CiConfig {

    private int port = 8008;
    
    //脚本处理-扫描路径
    private String handlerDir = "bin/";
    //脚本处理-扫描包
    private String handlerPackage = "ci.app";
    
    //文件目录
    private String fileDir = "www/";
    //文件路由路径
    private String filePath = "/";
    //文件缓存时间
    private int fileMaxAge = 604800;
    //排除某些后缀
    private String exceptExtend = null;
    
    private int maxMsgSize = 64*1024;
    //cors配置
    private String corsOrgins = null;
    //限制最大连接数
    private int limit = 0;
    //工作线程数
    private int workThread = 0;
    
    //允许哪些域名指向
    private String host;
    
    //是否启用websocket
    private boolean webSocket;
    private String webSocketPath="/websocket";
    
    public CiConfig() {
    }
    
    public String handlerDir(){
        return handlerDir;
    }
    /**
     * 设置web-api的类路径(可以是文件目录，可以是jar文件)
     * @param p
     * @return
     */
    public CiConfig handlerDir(String p){
        handlerDir = p;
        return this;
    }
    public String handlerPackage(){
        return handlerPackage;
    }
    /**
     * 设置web-api的包路径前缀, 例如统一在  ci.app
     * @param p
     * @return
     */
    public CiConfig handlerPackage(String p){
        handlerPackage = p;
        return this;
    }
    
    public String fileDir(){
        return fileDir;
    }
    /**
     * 设置静态文件 磁盘路径
     * @param p
     * @return
     */
    public CiConfig fileDir(String p){
        fileDir = p;
        return this;
    }
    /**
     * 设置静态文件请求路由
     * @param p
     * @return
     */
    public CiConfig filePath(String p) {
        filePath = p;
        return this;
    }
    public String filePath() {
        return filePath;
    }
    /**
     * 设置排除哪些后缀，排除后，不作为静态文件路由
     * @param p
     * @return
     */
    public CiConfig exceptExtend(String p) {
        exceptExtend = p;
        return this;
    }
    public String exceptExtend() {
        return exceptExtend;
    }
    /**
     * 设置静态文件 在浏览器内的缓存时间，单位是秒
     * @param p
     * @return
     */
    public CiConfig fileMaxAge(int p) {
        fileMaxAge = p;
        return this;
    }
    public int fileMaxAge() {
        return fileMaxAge;
    }
    
    public int limit(){
        return limit;
    }
    /**
     * 设置最大同时允许多少个socket连接
     * @param p
     * @return
     */
    public CiConfig limit(int p){
        limit = p;
        return this;
    }
    public int workThread(){
        return workThread;
    }
    /**
     * 设置多少个工作线程
     * @param p
     * @return
     */
    public CiConfig workThread(int p){
        workThread = p;
        return this;
    }
    
    public int maxMsgSize(){
        return maxMsgSize;
    }
    /**
     * 设置http请求允许的最大数据长度
     * @param p
     * @return
     */
    public CiConfig maxMsgSize(int p){
        maxMsgSize = p;
        return this;
    }
    
    public int port(){
        return port;
    }
    /**
     * 设置绑定的端口
     * @param p
     * @return
     */
    public CiConfig port(int p){
        port = p;
        return this;
    }
    
    public String corsOrgins() {
        return corsOrgins;
    }
    /**
     * 设置html5跨域请求处理域名
     * <pre>
     *    config.corsOrgins("baidu.com;qq.com");
     * </pre>
     * @param h
     * @return
     */
    public CiConfig corsOrgins(String orgins) {
        corsOrgins = orgins;
        return this;
    }
    public String host() {
        return host;
    }
    /**
     * 设置限定请求域名，以;,两种符号的任意一种分割，例如
     * <pre>
     *    config.host("baidu.com;qq.com");
     * </pre>
     * @param h
     * @return
     */
    public CiConfig host(String h) {
        this.host = h;
        return this;
    }
    
    
    public boolean webSocket() {
        return webSocket;
    }
    /**
     * 设置是否启用websocket功能
     * @param h
     * @return
     */
    public CiConfig webSocket(boolean h) {
        this.webSocket = h;
        return this;
    }
    public String webSocketPath() {
        return webSocketPath;
    }
    /**
     * 设置websocket连接路径
     * @param h
     * @return
     */
    public CiConfig webSocketPath(String h) {
        this.webSocketPath = h;
        return this;
    }
    
    /**
     * 读取配置
     * @param file
     * @return
     */
    public static CiConfig load(String file) {
        JSONObject obj = ConfigUtil.loadAsJson(file);
        if(obj != null) {
            return ConfigUtil.cast(obj, CiConfig.class);
        }
        return null;
    }
    
}
