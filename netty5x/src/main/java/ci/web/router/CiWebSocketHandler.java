package ci.web.router;

import ci.web.core.CiWebService;
import io.netty.channel.Channel;

/**
 * websocket-接口
 * @author zhh
 */
public interface CiWebSocketHandler {
    /**
     * 设置webservice
     * @param service
     */
    void init(CiWebService service);
    /**
     * 有socket连接
     * @param channel
     */
    void onOpen(Channel channel);
    /**
     * 收到文本消息
     * @param channel
     * @param data
     */
    void onText(Channel channel, String data);
    /**
     * 收到二进制消息
     * @param channel
     * @param bytes
     */
    void onBytes(Channel channel, byte[] bytes);
    /**
     * socket断开
     * @param channel
     * @param lost    是否客户端关闭
     */
    void onClose(Channel channel, boolean lost);
}
