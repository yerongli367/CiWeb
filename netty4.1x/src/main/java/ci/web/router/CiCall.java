package ci.web.router;

import ci.web.core.CiContext;

/**
 * ci-control-call-interface
 * @author zhh
 */
public interface CiCall {
    /**
     * ci-handler-call
     * @param context   
     * @return
     * @throws Exception
     */
    boolean call(CiContext context) throws Exception;
}
