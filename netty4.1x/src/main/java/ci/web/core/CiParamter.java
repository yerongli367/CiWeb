package ci.web.core;


import com.alibaba.fastjson.JSONObject;

/**
 * ci-request-paramter
 * @author zhh
 */
class CiParamter extends JSONObject {

    private final JSONObject post;
    private final JSONObject get;

    /**
     * 默认无参数
     */
    public CiParamter() {
        super(0);
        this.post = new JSONObject(0);
        this.get = new JSONObject(0);
    }
    /**
     * get请求参数
     * @param get
     */
    public CiParamter(JSONObject get){
        super(get);
        this.get = get;
        this.post = new JSONObject(0);
    }
    /**
     * post请求参数
     * @param post
     * @param get
     */
    public CiParamter(JSONObject post, JSONObject get) {
        super(get);
        this.putAll(post);
        this.post = post;
        this.get = get;
    }
    
    /**
     * post参数
     * @return
     */
    public JSONObject post(){
        return this.post;
    }
    /**
     * get参数
     * @return
     */
    public JSONObject get(){
        return this.get;
    }

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
}
