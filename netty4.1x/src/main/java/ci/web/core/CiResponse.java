package ci.web.core;

import java.io.File;

import com.alibaba.fastjson.JSONAware;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.cookie.Cookie;

/**
 * 响应
 * @author zhh
 */
public interface CiResponse {
    
    /**
     * 设置响应状态
     * @param status
     */
    void setStatus(HttpResponseStatus status);
    /**
     * 设置响应状态
     * @param statusCode
     * @param statusInfo
     */
    void setStatus(int statusCode, CharSequence statusInfo);
    /**
     * 设置响应内容类型
     * @param contentType
     */
    void setContentType(CharSequence contentType);
    /**
     * 设置响应http头
     * @param name
     * @param value
     */
    void setHeader(CharSequence name, CharSequence value);
    /**
     * 设置cookie
     * @param cookie
     */
    void setCookie(Cookie cookie);
    /**
     * 设置cookie
     * @param name      cookie-key
     * @param value     cookie-value
     */
    void setCookie(String name, String value);
    /**
     * 设置cookie
     * @param name      cookie-key
     * @param value     cookie-value
     * @param maxAge    最大有效时间
     */
    void setCookie(String name, String value, long maxAge);
    /**
     * 设置cookie
     * @param name      cookie-key
     * @param value     cookie-value
     * @param maxAge    最大有效时间
     * @param path      作用路径
     * @param domain    作用域
     * @param secure    只能通过https安全传输
     * @param httpOnly  不允许js获取cookie
     */
    void setCookie(String name, String value, long maxAge, String path, String domain, boolean secure, boolean httpOnly);
    
    /**
     * 重定向响应
     * @param location
     */
    void redirect(CharSequence location);
    /**
     * 重定向响应
     * @param location
     */
    void redirect(CharSequence location, int httpStatusCode);
    
    /**
     * 错误响应
     * @param errorInfo
     */
    void sendError(CharSequence errorInfo);
    /**
     * 错误响应
     * @param errorInfo
     */
    void sendError(int code, CharSequence errorInfo);
    
    /**
     * 响应文件
     * @param file
     */
    void send(File file);
    /**
     * 响应文件
     * @param file
     * @param fileCacheSecond
     */
    void send(File file, int fileCacheSecond);
    /**
     * 响应json数据
     * @param info
     */
    void send(JSONAware info);
    
    /**
     * 响应字符串
     * @param body
     */
    void send(CharSequence body);
    /**
     * 响应字节数组
     * @param body
     */
    void send(byte[] body);
    
    /**
     * 是否已写响应数据
     * @return
     */
    boolean isWroteBody();
    
    /**
     * 结束响应（打包所有的响应数据，发送）
     */
    void finish();
    /**
     * 是否结束
     * @return
     */
    boolean isFinish();
    
    /**
     * 清空响应内容并结束
     */
    void cleanFinish();
    /**
     * 获取ChannelContext，用于异步读写等之用
     * @return
     */
    ChannelHandlerContext ioContext();
}
