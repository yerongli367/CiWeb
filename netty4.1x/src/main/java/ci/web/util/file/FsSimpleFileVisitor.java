package ci.web.util.file;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Map;

/**
 * FileVisitor
 * @author zhangheng
 */
class FsSimpleFileVisitor extends SimpleFileVisitor<Path> {

	private final Map<WatchKey, Path> keyMap;
	private final WatchService watchService;
	public FsSimpleFileVisitor(WatchService watchService, Map<WatchKey, Path> keyMap){
		this.watchService = watchService;
		this.keyMap = keyMap;
	}
	
	@Override
	public FileVisitResult preVisitDirectory(Path path, BasicFileAttributes attrs)
			throws IOException {
    	WatchKey key = path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE,  
                StandardWatchEventKinds.ENTRY_MODIFY,  
                StandardWatchEventKinds.ENTRY_DELETE);
    	keyMap.put(key, path);
        return FileVisitResult.CONTINUE;
		//return super.visitFile(file, attrs);
	}

}
