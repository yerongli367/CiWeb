package ci.web.annotaction;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 内部类/方法 注释。<br/>
 * 隐藏ci-app扫描包内的class/method/constructor，使其不作为路由扫描<br/>
 * 如果对类注释，忽略其所有方法。<br/>
 * 也可以写 [inner.txt]文件放在包内，则隐藏整个包
 * @author zhh
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.METHOD,ElementType.CONSTRUCTOR})
public @interface Inner {

}
