package ci.web;

/**
 * netty-系统配置
 * @author zhh
 */
public class CiSystemConfig {

    /**
     * 是否支持http-alive
     */
    public static boolean HttpKeepAlive = true;
    /**
     * socket发送缓冲大小
     */
    public static int SocketSndbufSize = 8192;
    /**
     * socket接收缓冲大小
     */
    public static int SocketRcvbufSize = 8192;
    /**
     * socket-BACKLOG
     */
    public static int SocketBackLog = 256;
    /**
     * So_keepalive
     */
    public static boolean SocketKeepAlive = false;
    /**
     * So_ReuseAddr
     */
    public static boolean SocketReuseAddr = true;
    /**
     * tcp-nodelay
     */
    public static boolean TCP_NODELAY = true;

}
